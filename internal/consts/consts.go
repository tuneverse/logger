package consts

// app name
const (
	AppName = "logger"
)

// mongoDB collection names
const (
	CollectionLogs = "logs"
)

const (
	HoursInADay = 24
	UserID      = "data.user_id"
	PartnerID   = "data.partner_id"
)
