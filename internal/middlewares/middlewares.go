package middlewares

// Middlewares is an empty struct representing a container for middleware configurations.
type Middlewares struct {
}

// NewMiddlewares creates a new instance of the Middlewares struct.
func NewMiddlewares() *Middlewares {
	return &Middlewares{}
}
